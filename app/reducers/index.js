import { combineReducers } from 'redux'
import AuthReducer from './authReducer'
import TestReducer from './TestReducer'

const rootReducer = combineReducers({
    AuthReducer,
    TestReducer
})

export default rootReducer