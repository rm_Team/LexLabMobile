import * as types from '../types'

const initialState = {
    user: {},
    avatar: '',
    login: '',
    loggedIn: false
}

// Здесь исключительно пишется то, что связано с авторизацией

export default function dataReducer(state = initialState, action) {
    switch(action.type) {
        case types.LOGIN_CHANGED:
            return { ...state, email: action.login }
        case types.FETCHING_USER:
            return {
                ...state
            }
        case types.UPDATE_USER:
            return {
                ...state,
                user: action.data
            }
        case types.FETCHING_AVATAR:
            return {
                ...state
            }
        case types.UPDATE_AVATAR:
            return {
                ...state,
                avatar: action.data
            }
        default:
            return state
    }
}