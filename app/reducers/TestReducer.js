import {
  FETCH_TEST_NEW,
  FETCH_TEST_ALL
} from '../types'

const INITIAL_STATE = {
  newLesson: {},
  allLesson: {}
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_TEST_NEW:
      return { ...state, newLesson: action.payload }
    case FETCH_TEST_ALL:
      return { ...state, allLesson: action.payload }
    default:
      return state
  }
}