import {
    LOGIN_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_STATE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    LOGIN_USER_ERROR,
    FETCH_USER_INFO
} from '../types'

const INITIAL_STATE = {
    user: {},
    avatar: {},
    currentGroup: {},
    token: '',
    loggedIn: false,
    loginError: false
}

// Здесь исключительно пишется то, что связано с авторизацией

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN_USER_STATE:
            return { ...state, loggedIn: action.payload }
        case FETCH_USER_INFO:
            //console.log(action.payload.user)
            return { ...state, user: action.payload.user, avatar: action.payload.avatar, currentGroup: action.payload.currentGroup }
        case LOGIN_USER_ERROR:
            return { ...state, loginError: true }
        case LOGIN_USER_SUCCESS:
            return { ...state, token: action.payload.token, user: action.payload.user, avatar: action.payload.avatar, loggedIn: true, loginError: false }
        case LOGOUT_USER:
            return { ...state, user: {}, avatar: {}, loggedIn: false, token: '' }
        default:
            return state
    }
}