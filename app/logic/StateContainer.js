import React, { Component } from 'react'
import { connect, getState } from 'react-redux'
import RootNavigator from '../config/rootRoutes'
import MenuNavigator from '../config/userRoutes'
import { checkLoginState, fetchUserInfo } from '../actions'
import configureStore from './../config/configureStore'
import { AsyncStorage } from 'react-native';

const store = configureStore()

class StateContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: '',
            loggedIn: false,
            currentGroup: [],
            userData: {}
        }

        this.changeLoginState = this.changeLoginState.bind(this)
    }

    async componentWillMount() {
        const jwt = await AsyncStorage.getItem('@easylexlab:jwt')

        store.dispatch(checkLoginState(jwt))
        
        this.setState({
            loggedIn: store.getState().AuthReducer.loggedIn
        })
    }

    changeLoginState() {
        this.setState({ loggedIn: !this.state.loggedIn })
    }

    userAuth() {
        switch (this.state.loggedIn) {
            case true:
                return <MenuNavigator screenProps={[
                    this.changeLoginState,
                    this.state.userData
                ]} />
            case false:
                return <RootNavigator screenProps={[
                    this.changeLoginState,
                    store.getState().AuthReducer
                ]} />
            default:
                return (
                    <View
                        style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                    >
                        <ActivityIndicator color='#FF5858' size='large' />
                    </View>
                )
        }
    }

    render() {
        return this.userAuth()
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        token: state.token,
        loggedIn: state.loggedIn,
        avatar: state.avatar,
        jwt: state.jwt,
        loginError: state.loginError
    }
}

export default connect(
    mapStateToProps,
    { checkLoginState, fetchUserInfo }
)(StateContainer)