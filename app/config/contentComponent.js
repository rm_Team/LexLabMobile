import React, { Component } from 'react'
import { ScrollView, View, Text, TouchableOpacity, AsyncStorage, Image, Picker } from 'react-native'
import { DrawerItems } from 'react-navigation'
import { connect, getState } from 'react-redux'
import { fetchUserInfo } from '../actions'
import styles from '../common/indexStyles'
import configureStore from '../config/configureStore'

const store = configureStore()

class contentComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {},
      avatar: {},
      selectGroup: {},
      groups: []
    }
  }
  
  async componentWillMount() {
    const jwt = await AsyncStorage.getItem('@easylexlab:jwt')
    store.dispatch(fetchUserInfo(jwt))
    const groupStorage = await AsyncStorage.getItem('@easylexlab:group')
    this.setState({
      selectGroup: {
        name: groupStorage,
        id: ''
      }
    })
  }
  
  async componentDidMount() {
    store.subscribe(() => {
      this.setState({
        user: store.getState().AuthReducer.user,
        avatar: store.getState().AuthReducer.avatar,
        groups: store.getState().AuthReducer.user._groups
      })
    })
  }

  render() {
    const { firstName, lastName, wordsLearnt } = this.state.user
    const base64Icon = 'data:image/png;base64,' + this.state.avatar.img
    const { groups } = this.state

    return (
      <ScrollView style={styles.container}>
          <Text style={styles.learn}>
            Выученных слов: {wordsLearnt}
          </Text>
          <View style={styles.centerItems}>
              <Image
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  resizeMode: 'cover',
                  borderWidth: 0
                }}
                source={{uri: base64Icon}}
              />
              <Text style={styles.name}>
                  {firstName} {lastName}
              </Text>
          </View>
          <DrawerItems {...this.props} />
          <View style={styles.pickerStyle}>
            <Picker
              style={styles.picker}
              itemStyle={styles.pickerItem}
              selectedValue={this.state.selectGroup.name}
              onValueChange={async (itemValue, itemIndex) => {
                try {
                  const group = await AsyncStorage.getItem('@easylexlab:group')
                  if (group !== null && group != undefined && this.state.selectGroup.name != group) {
                    // cool
                  } else {
                    await AsyncStorage.setItem('@easylexlab:group', itemValue)
                    this.setState({selectGroup: { name: itemValue, id: itemIndex }})
                  }
                } catch (error) {
                  console.log(error)
                }
              }}
            >
              {groups.map((item, index) => (<Picker.Item label={item.name} key={index} value={item.name} />))}
            </Picker>
          </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  avatar: state.avatar,
  selectGroup: state.selectGroup,
  groups: state.groups
})

export default connect(
  mapStateToProps,
  { fetchUserInfo }
)(contentComponent)