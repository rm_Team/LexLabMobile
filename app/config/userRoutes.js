import React, { Component } from 'react'
import { StackNavigator, DrawerNavigator, DrawerItems, TabNavigator } from 'react-navigation'
import { Ionicons } from '@expo/vector-icons'
import {
    Text,
    View,
    ActivityIndicator,
    Platform,
    StatusBar,
    TouchableOpacity,
    AsyncStorage,
    Dimensions
} from 'react-native'
import Messages from '../screen/Messages'
import Result from '../screen/Result'
import Settings from '../screen/Settings'
import { logout } from '../actions'
import configureStore from '../config/configureStore'

import AllLessons from '../screen/user/AllLessons'
import NewLessons from '../screen/user/NewLessons'

import Learn from '../screen/task/Learn'
import Play from '../screen/task/Play'
import Tester from '../screen/task/Tester'

import Flashcards from '../screen/task/games/Flashcards'
import Matching from '../screen/task/games/Matching'
import Scramble from '../screen/task/games/Scramble'
import Snake from '../screen/task/games/Snake'
import Typein from '../screen/task/games/Typein'
import contentComponent from './contentComponent'

const store = configureStore()

const HomeTab = TabNavigator({
    NewLessons: {
        screen: (props) => <NewLessons {...props} />,
        navigationOptions: {
            title: 'НОВЫЕ'
        }
    },
    AllLessons: {
        screen: (props) => <AllLessons {...props} />,
        navigationOptions: {
            title: 'ВЫПОЛНЕННЫЕ'
        }
    },
}, {
    tabBarPosition: 'top',
    animationEnabled: false,
    tabBarOptions: {
        ...Platform.select({
            ios: {
                activeTintColor: '#8B97E1',
                inactiveTintColor: '#9D9D9D',
                showIcon: false,
                borderWidth: 1,
                style: {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                },
                labelStyle: {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontSize: 16,
                }
            },
            android: {
                activeTintColor: '#8B97E1',
                inactiveTintColor: '#9D9D9D',
                showIcon: false,
                borderWidth: 0,
                style: {
                    backgroundColor: '#F5F5F5'
                },
                indicatorStyle: {
                    backgroundColor: '#8B97E1'
                },
                labelStyle: {
                    fontSize: 12
                }
            },
        })
    }
})

const TabGamesNav = TabNavigator({
    Learn: {
        screen: Learn
    },
    Play: {
        screen: Play
    },
    Tester: {
        screen: Tester
    }
}, {
    tabBarPosition: 'top',
    animationEnabled: true,
    tabBarOptions: {
        activeTintColor: '#8B97E1',
        inactiveTintColor: '#9D9D9D',
        showIcon: false,
        borderWidth: 1,
        style: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        labelStyle: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: 16,
        }
    }
})

const ProfileStack = StackNavigator({
    Profile: {
        screen: HomeTab,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-menu" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.navigate('DrawerOpen')} />,
            title: 'Главная',
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Test: {
        screen: TabGamesNav,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            title: navigation.state.params.test.name,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Flashcards: {
        screen: Flashcards,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Matching: {
        screen: Matching,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Scramble: {
        screen: Scramble,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Snake: {
        screen: Snake,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    },
    Typein: {
        screen: Typein,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-arrow-back" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.goBack(null)} />,
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    }
}, {
    cardStyle: {
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
})

const MessagesStack = StackNavigator({
    Messages: {
        screen: Messages,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-menu" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.navigate('DrawerOpen')} />,
            title: 'Сообщения',
            headerTintColor: '#3F3F3F',
            headerTitleStyle: {
                fontWeight: '400',
                fontSize: 18
            },
            headerStyle: {
                backgroundColor: '#FAFAFA',
                shadowOffset: {
                    height: 0
                },
                shadowOpacity: 0,
                shadowRadius: 0,
                elevation: 0,
            }
        })
    }
}, {
    cardStyle: {
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
})

const ResultStack = StackNavigator({
    Result: {
        screen: Result,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-menu" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.navigate('DrawerOpen')} />
        })
    }
}, {
    cardStyle: {
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
})

const SettingsStack = StackNavigator({
    Settings: {
        screen: Settings,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Ionicons name="ios-menu" style={{ marginLeft: 25 }} size={30} color="#CDCDCD" onPress={() => navigation.navigate('DrawerOpen')} />
        })
    }
}, {
    cardStyle: {
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
})

const MenuNavigator = DrawerNavigator({
    Profile: {
        screen: ProfileStack
    },
    Messages: {
        screen: MessagesStack
    },
    Result: {
        screen: ResultStack
    },
    Settings: {
        screen: SettingsStack
    }
}, {
    headerMode: 'screen',
    drawerBackgroundColor: '#FAFAFA',
    contentComponent: contentComponent,
    contentOptions: {
        activeTintColor: '#FFF',
        inactiveTintColor: '#9E9E9E',
        activeBackgroundColor: '#8B97E1',
        inactiveBackgroundColor: '#FFF',
        labelStyle: {
            fontWeight: '400'
        },
        itemsStyle: {
            borderBottomWidth: 1,
            borderColor: '#FAFAFA'
        }
    }
})

const UserNavigator = StackNavigator({
    Main: {
        screen: (props) => <MenuNavigator screenProps={this.props} />
    }
})

export default MenuNavigator