import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'

import Home from '../screen/Home'
import Login from '../screen/Login'
import Typein from '../screen/task/games/Typein';



const RootNavigator = StackNavigator({
    Home: {
        screen: Home
    },
    Login: {
        screen: Login
    }
})

export default RootNavigator