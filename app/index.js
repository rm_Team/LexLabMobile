import React, { Component } from 'react'
import {
  Text,
  View,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  AppRegistry
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import axios from 'axios'
import { Provider } from 'react-redux'
import configureStore from './config/configureStore'
import StateContainer from './logic/StateContainer'

EStyleSheet.build({
  $error: '#CE4257',
  $success: '#7EB77F'
})

const store = configureStore()

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StateContainer/>
      </Provider>
    )
  }
}
