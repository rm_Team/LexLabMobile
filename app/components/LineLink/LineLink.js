import React, { Component } from 'react'
import { Text, TouchableHighlight, View } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import style from './style'

export default class LineLink extends Component {
  render() {
    const {
        main,
        wrapper,
        nameAndResult,
        name,
        percent,
        percentText
    } = style

    return (
        <TouchableHighlight underlayColor="rgb(139, 151, 225)" style={main} onPress={this.props.onPress}>
            <View style={wrapper}>
                <View style={nameAndResult}>
                    <Text style={name}>{this.props.title}</Text>
                    {
                        this.props.done ? 
                            <View style={percent}>
                                { this.props.attempts && <Text style={percentText}>{this.props.attempts}</Text> }
                                { this.props.percentage && <Text style={percentText}>{this.props.percentage}%</Text> }
                            </View> : <Text></Text>
                    }
                </View>
                <Ionicons name="ios-arrow-forward" color="rgb(201, 201, 205)" size={25} />
            </View>
        </TouchableHighlight>
    )
  }
}