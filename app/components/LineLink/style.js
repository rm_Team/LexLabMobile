import EStyleSheet from 'react-native-extended-stylesheet'

const style = EStyleSheet.create({
    main: {
        flexDirection: 'row',
        backgroundColor: 'white',
        marginTop: 20,
    },
    wrapper: {
        width: '100%',
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    nameAndResult: {
        flex: 0.8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    name: {
        fontSize: 16,
        color: 'rgb(144, 144, 144)',
        fontWeight: 'bold',
        marginRight: 10
    },
    percent: {
        borderRadius: 10,
        backgroundColor: 'rgb(245, 245, 245)',
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    percentText: {
        color: 'rgb(155, 155, 155)',
        fontWeight: 'bold',
        fontSize: 14
    }
})

export default style