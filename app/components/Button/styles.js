import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
    textCenter: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    blueButton: {
        backgroundColor: 'rgb(53, 137, 216)',
        width: 250,
        padding: 10,
        borderRadius: 5
    },
    redButton: {
        backgroundColor: 'rgb(242, 103, 47)',
        width: 250,
        padding: 10,
        borderRadius: 5
    }
})

export default styles