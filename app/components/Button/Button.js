import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class Button extends Component {
    render() {
        const { blueButton, redButton, textCenter } = styles
        return (
            <TouchableOpacity
                style={[
                    this.props.blue && blueButton,
                    this.props.red && redButton,
                    this.props.gray && { backgroundColor: 'rgb(204, 204, 204)' },
                    this.props.width && { width: this.props.width },
                    this.props.padding && { padding: this.props.padding }
                ]}
                onPress={this.props.handler}
                disabled={this.props.disabled}
            >
                <Text style={[
                    textCenter, 
                    this.props.fontSize && { fontSize: this.props.fontSize }
                    ]}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}