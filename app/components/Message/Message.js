import React, { Component } from 'react'
import { Text, View } from 'react-native'

import styles from './styles'

export default class Input extends Component {
    constructor() {
      super()
    }

    render() {
      const {
        container,
        text,
        author,
        image,
        block,
        textBlock
      } = styles

      return (
          <View style={container}>
            <View style={block}>
              <View style={image}></View>
              <Text style={author}>{this.props.author}</Text>
            </View>
            <View style={textBlock}>
              <Text style={text}>{this.props.text}</Text>
            </View>
          </View>
      )
    }
}