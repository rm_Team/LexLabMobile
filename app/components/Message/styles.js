import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    marginHorizontal: 15,
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: '#FFF',
  },
  block: {
    borderBottomWidth: 1,
    borderBottomColor: '#F1F1F1',
    paddingBottom: 15,
    width: '100%',
    flexDirection: 'row'
  },
  textBlock: {
    marginTop: 15
  },
  image: {
    borderRadius: 20,
    backgroundColor: '#222',
    width: 40,
    height: 40
  },
  author: {
    paddingLeft: 20,
    textAlign: 'left',
    fontSize: 16,
    lineHeight: 30
  },
  text: {
    fontSize: 14,
    textAlign: 'left',
    fontWeight: '400'
  }
})

export default styles