import React, { Component } from 'react'
import { Text, View, TextInput } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import styles from './style'

export default class Input extends Component {
    constructor() {
        super()
        this.state = {
            text: ''
        }
    }

    render() {
        return (
            <View
                style={[
                    this.props.last ? styles.containerNoBorder : styles.container,
                    this.props.borderSize && { borderBottomWidth: this.props.borderSize, },
                    this.props.color && { borderBottomColor: this.props.color }
                ]}>
                <TextInput
                    secureTextEntry={this.props.password ? true : false}
                    keyboardType={this.props.email ? 'email-address' : 'default'}
                    style={styles.input}
                    placeholderTextColor={this.props.error ? '#CE4257' : 'lightgray'}
                    underlineColorAndroid='transparent'
                    onChangeText={text => this.props.handler(text, this.props.property)}
                    placeholder={this.props.placeholder}
                />
            </View>
        )
    }
}