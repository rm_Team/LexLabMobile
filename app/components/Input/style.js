import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        height: 40,
        borderBottomColor: 'rgb(219, 219, 219)', 
        borderBottomWidth: 1,
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
    },
    containerNoBorder: {
        height: 40,
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
    },
    input: {
        height: 40,
        fontSize: 14
    },
})

export default styles