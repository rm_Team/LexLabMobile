import React, { Component } from 'react'
import {
    Animated,
    Easing,
    Text,
    View,
    Platform,
} from 'react-native'
import SortableList from 'react-native-sortable-list'
import EStyleSheet from 'react-native-extended-stylesheet'

export default class Row extends Component {

    constructor(props) {
        super(props)

        this._active = new Animated.Value(0)

        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            Animated.timing(this._active, {
                duration: 300,
                easing: Easing.bounce,
                toValue: Number(nextProps.active),
            }).start()
        }
    }

    render() {
        const { data, active } = this.props

        return (
            <Animated.View style={[styles.row, this._style]}>
                <Text style={styles.text}>{data.letter}</Text>
            </Animated.View>
        );
    }
}

const styles = EStyleSheet.create({
    row: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#BCE7FD',
        width: 30,
        height: 30,
        marginRight: 10,
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: { height: 2, width: 2 },
                shadowRadius: 2,
            },
            android: {
                elevation: 0,
                marginHorizontal: 30,
            },
        })
    },
    text: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#222222',
    },
})