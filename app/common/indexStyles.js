import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
    main: {
        paddingTop: 20,
        paddingLeft: 0,
        paddingRight: 0,
        backgroundColor: 'rgb(250, 250, 250)',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        paddingTop: 40
    },
    learn: {
        fontSize: 12,
        color: '#3F3F3F',
        textAlign: 'center',
        fontWeight: '400',
        marginBottom: 20
    },
    centerItems: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 30
    },
    name: {
        color: '#333',
        fontWeight: '600',
        paddingTop: 40,
        fontSize: 18
    },
    btnItem: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 4,
    },
    btnStyle: {
        alignItems: 'center',
        borderRadius: 10,
        width: 250,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#8B97E1'
    },
    btnTextStyle: {
        color: '#FFF',
        fontWeight: '400'
    },
    pickerStyle: {
        backgroundColor: '#FFF'
    },
    picker: {
        marginHorizontal: 10
    },
    pickerItem: {
        fontSize: 14,
        color: '#3F3F3F'
    }
})

export default styles