import React, { Component } from 'react'
import { Text, ScrollView, View, TouchableOpacity } from 'react-native'
import Expo from 'expo'
import { connect, getState } from 'react-redux'
import { logout } from '../actions'
import styles from '../common/indexStyles'
import configureStore from '../config/configureStore'

const store = configureStore()

class Settings extends Component {
    constructor() {
        super()
        this.state = {
            isReady: false
        }
    }

    async componentWillMount() {
        await Expo.Font.loadAsync({
            RobotoRegular: require("../../assets/fonts/Roboto/Roboto-Bold.ttf")
        })
        this.setState({ isReady: true })
    }

    static navigationOptions = {
        title: 'Настройки',
        headerLeft: null,
        headerTintColor: '#3F3F3F',
        headerTitleStyle: {
            fontWeight: '400',
            fontSize: 18
        },
        headerStyle: {
            backgroundColor: '#FAFAFA',
            shadowOffset: {
                height: 0
            },
            shadowOpacity: 0,
            shadowRadius: 0,
            elevation: 0,
        }
    }

    render() {
        if (!this.state.isReady) {
            return <Expo.AppLoading />
        }

        return (
            <ScrollView style={{flex: 1, paddingTop: 15}}>
              <View style={{backgroundColor: '#FFF'}}>
                <View>
                  <Text>настройки</Text>
                </View>
                <View style={styles.btnItem}>
                    <TouchableOpacity
                        style={styles.btnStyle}
                        onPress={() => {
                            store.dispatch(logout())
                            this.props.screenProps[0]()
                        }}
                    >
                        <Text style={styles.btnTextStyle}>Выход</Text>
                    </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
        );
    }
}

export default connect(
    null,
    { logout }
  )(Settings)