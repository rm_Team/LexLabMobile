import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { TabNavigator } from 'react-navigation'
import EStyleSheet from 'react-native-extended-stylesheet'

import Learn from './task/Learn'
import Play from './task/Play'
import Test from './task/Test'

const TabNav = TabNavigator({
    Learn: {
        screen: Learn
    },
    Play: {
        screen: Play
    },
    Test: {
        screen: Test
    }
}, {
    tabBarPosition: 'top',
    animationEnabled: true,
    tabBarOptions: {
        activeTintColor: '#8B97E1',
        inactiveTintColor: '#9D9D9D',
        style: {
            backgroundColor: '#F5F5F5'
        },
        indicatorStyle: {
            backgroundColor: '#8B97E1',
        },
        labelStyle: {
            fontSize: 18,
        }
    }
})

export default class Task extends Component {
    static navigationOptions = {
        title: 'Урок 1'
    }
    
    render() {
        const {
            main,
        } = styles

        return (
            <View style={main}>
                <TabNav/>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
})