import React, { Component } from 'react'
import { Text, View } from 'react-native'
import LineLink from '../../components/LineLink'

export default class Learn extends Component {
  static navigationOptions = {
    tabBarLabel: 'ТЕСТ'
  }

  render() {
    return (
      <View>
        <LineLink title="Тест" />
      </View>
    )
  }
}