import React, { Component } from 'react'
import { Text, View } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Input from '../../../components/Input'
import Button from '../../../components/Button'

export default class Typein extends Component {
    constructor() {
        super()
        this.state = {
            currentPair: {
                key: 'english blin',
                def: 'Слово'
            },
            type: '',
            totalAttempts: 0,
            doneAttempts: 0,
            error: false
        }
        this.submit = this.submit.bind(this)
        this.inputHandler = this.inputHandler.bind(this)
    }

    static navigationOptions = {
        title: 'Впиши слово'
    }

    inputHandler(text, kek) {
        this.setState({ type: text, error: false })
    }

    submit() {
        if(this.state.type == this.state.currentPair.key) {
            this.setState({ error: false }, () => {
            })
            // TODO
            return
        }
        this.setState({ error: true }, () => {
        })
    }

    componentDidMount() {
        this.setState({ totalAttempts: this.props.navigation.state.params.testSingle.attempts.typein })
    }

    render() {
        const {
            main,
            gameWrapper,
            inputContainer,
            wordKey
        } = styles

        return (
            <View style={main}>
                <Text style={{ fontSize: 18, marginBottom: 30, fontWeight: 'bold' }}>Пройдено раз: 0/1</Text>
                <View style={gameWrapper}>
                    <View style={inputContainer}>
                        <Text style={wordKey}>{this.state.currentPair.def}</Text>
                        <Input
                            borderSize={2}
                            error={this.state.error}
                            color={this.state.error ? '#F56476' : false}
                            placeholder="Перевод"
                            handler={this.inputHandler}
                        />
                    </View>
                    { this.state.error && <Text style={{ color: '#F56476', marginBottom: 20, fontWeight: 'bold', fontSize: 18 }}>Неверно! Исправь ошибку!</Text> }
                    <Button
                        red
                        gray={this.state.error}
                        disabled={this.state.error}
                        title="Дальше"
                        width="80%"
                        handler={this.submit}
                    />
                </View>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 30
    },
    gameWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        padding: 0
    },
    inputContainer: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginBottom: 30,
        flexWrap: 'wrap'
    },
    wordKey: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#007090',
    }
})