import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import _ from 'lodash'

import Button from '../../../components/Button'

export default class Flashcards extends Component {
    constructor() {
        super()
        this.state = {
            showDef: false,
            otalAttempts: 0,
            doneAttempts: 0,
            words: [],
            currentWordGroup: {
                key: '',
                value: ''
            },
            win: false,
            done: false,
            correct: 0,
            msg: '',
            percentage: 0
        }

        this.changeCurrentWordGroup = this.changeCurrentWordGroup.bind(this)
        this.restart = this.restart.bind(this)
        this.toKnow = this.toKnow.bind(this)
        this.toDontKnow = this.toDontKnow.bind(this)
    }

    static navigationOptions = {
        title: 'Выучи слова'
    }

    changeCurrentWordGroup() {
        let currentIndex = this.state.words.indexOf(this.state.currentWordGroup)
        if (this.state.words.length - 1 != currentIndex) {
            currentIndex++
            this.setState({ currentWordGroup: this.state.words[currentIndex], showDef: false })
        } else {
            let percentage = Math.round(this.state.correct * 100 / this.state.words.length)
            if (percentage < 0)
                percentage = 0
            if (percentage >= 90) {
                if (percentage == 100) {
                    this.setState({ msg: 'Отлично!', win: true, doneAttempts: this.state.doneAttempts + 1, percentage })
                } else {
                    this.setState({ msg: 'Очень хорошо!', win: true, doneAttempts: this.state.doneAttempts + 1, percentage })
                }
            } else if (percentage < 90 && percentage >= 60) {
                this.setState({ msg: 'Хорошо, но необходимо пройти задание повторно!', done: true, percentage })
            } else if (percentage < 60) {
                this.setState({ msg: 'Пройди задание повторно!', done: true, percentage })
            }
        }
    }

    componentDidMount() {
        let words = _.shuffle(this.props.navigation.state.params.testSingle.tasks[0].content)
        this.setState({ totalAttempts: this.props.navigation.state.params.testSingle.attempts.flashcards, words, currentWordGroup: words[0] })
    }

    toKnow() {
        this.setState({ correct: this.state.correct + 1 })
        this.changeCurrentWordGroup()
    }

    toDontKnow() {
        this.changeCurrentWordGroup()
    }

    restart() {
        this.setState({ words: _.shuffle(this.state.words), correct: 0, percentage: 0 }, () => {
            this.setState({ currentWordGroup: this.state.words[0], done: false, win: false })
        })
    }

    render() {
        const {
            main,
            gameWrapper,
            wordContainer,
            word,
            buttonContainer
        } = styles

        return (
            <View style={main}>
                <View style={gameWrapper}>
                    <Text style={{ fontSize: 18, marginBottom: 30, fontWeight: 'bold' }}>Пройдено раз: {this.state.doneAttempts}/{this.state.totalAttempts}</Text>
                    {
                        !this.state.win && !this.state.done && (
                            <View style={{ flexDirection: 'column', width: '100%' }}>
                                <TouchableOpacity
                                    style={wordContainer}
                                    onPress={() => this.setState({ showDef: !this.state.showDef })}>
                                    {
                                        !this.state.showDef ?
                                            <Text style={word}>{this.state.currentWordGroup.key}</Text> :
                                            <Text style={word}>{this.state.currentWordGroup.value}</Text>
                                    }
                                </TouchableOpacity>
                                <View style={buttonContainer}>
                                    <Button
                                        red
                                        title="ЗНАЮ"
                                        handler={this.toKnow}
                                    />
                                    <Button
                                        blue
                                        title="НЕ ЗНАЮ"
                                        handler={this.toDontKnow}
                                    />
                                </View>
                            </View>
                        )
                    }
                    {  
                        (this.state.win || this.state.done) && (
                            <View style={{ flexDirection: 'column', alignItems: 'center', width: '80%' }}>
                                <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 10 }}>{ this.state.msg }</Text>
                                <Text style={{ fontSize: 14, marginBottom: 10 }}>Ваш результат: {this.state.percentage}%</Text>
                                <Button
                                    blue
                                    title="Перезапуск"
                                    width="100%"
                                    handler={this.restart}
                                />
                            </View>
                        )
                    }
                </View>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 30
    },
    gameWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        width: 250,
        padding: 0
    },
    wordContainer: {
        width: 250,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 150,
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginBottom: 30
    },
    buttonContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 90
    },
    word: {
        fontSize: 22,
        fontWeight: 'bold'
    }
})