import React, { Component } from 'react'
import { Text, View, PanResponder, Dimensions } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import _ from 'lodash'

import Button from '../../../components/Button'

let lastCoords = {}

    // UP = [-1, 0]
    // DOWN = [1, 0]
    // LEFT = [0, -1]
    // RIGHT = [0, 1]


export default class Snake extends Component {
    constructor() {
        super()
        this.state = {
            ms: 500,
            direction: [0, 1],
            wordGroups: [],
            currentWordGroup: {
                key: '',
                trueWord: '',
                words: [],
                word2: '',
                word3: ''
            },
            snakeLength: 5,
            snake: [],
            dead: false,
            tick: null,
            grid: Array(15).fill(0).map(item => Array(15).fill(0)),
            emptyGrid: Array(15).fill(0).map(item => Array(15).fill(0)),
            _panResponder: null,
            food: [],
            eaten: [],
            correct: [],
            win: false,
            totalAttempts: 0,
            doneAttempts: 0,
        }
        this.createSnake = this.createSnake.bind(this)
        this.setFood = this.setFood.bind(this)
        this.eatFood = this.eatFood.bind(this)
        this.moveSnake = this.moveSnake.bind(this)
        this.setGrid = this.setGrid.bind(this)
        this.restart = this.restart.bind(this)
        this.setWordGroups = this.setWordGroups.bind(this)
        this.changeCurrentWordGroup = this.changeCurrentWordGroup.bind(this)
        // this.createFood = this.createFood.bind(this)
    }

    setWordGroups(words) {
        let wordGroups = []
        for (let i = 0; i < words.length; i++) {
            let wordGroup = {}
            wordGroup.words = []
            for (let pair of _.shuffle(words)) {
                if (!wordGroup.key) {
                    wordGroup.key = pair.key
                    wordGroup.trueWord = pair.value
                    wordGroup.words.push(pair)
                    continue
                }
                if (!wordGroup.word2) {
                    wordGroup.word2 = pair.value
                    wordGroup.words.push(pair)
                    continue
                }
                if (wordGroup.word3)
                    break
                wordGroup.word3 = pair.value
                wordGroup.words.push(pair)
                wordGroup.words = _.shuffle(wordGroup.words)
            }
            wordGroups.push(wordGroup)
        }
        this.setState({ currentWordGroup: wordGroups[0], wordGroups, correct: [], eaten: [] })
    }

    changeCurrentWordGroup() {
        let currentIndex = this.state.wordGroups.indexOf(this.state.currentWordGroup)
        if (this.state.wordGroups.length - 1 != currentIndex) {
            currentIndex++
            setTimeout(() => {
                this.setState({ currentWordGroup: this.state.wordGroups[currentIndex], correct: [], eaten: [] })
                this.setFood()
            }, 2000)
        } else {
            this.setState({ win: true, doneAttempts: this.state.doneAttempts + 1 })
        }
    }

    createSnake() {
        let newSnake = []
        for(let i = this.state.snakeLength - 1; i >= 0; i--) {
            newSnake.push({ x: i, y: 0 })
        }
        let grid = this.setGrid()
        newSnake.map((cell, i) => {
            grid[cell.y].props.children[cell.x] = (
                <View key={i * 100} style={[styles.snake, i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                </View>
            )
        })
        let tick = setInterval(() => {
            if (this.props.navigation.state.routeName == 'Snake') {
                this.moveSnake()
            } else {
                if (this.state.correct.length > 0)
                    this.setState({ correct: [] })
                if (this.state.eaten.length > 0)
                    this.setState({ eaten: [] })
            }
        }, this.state.ms)
        this.setState({ snake: newSnake, grid, dead: false, tick, direction: [0, 1] }, () => {
            this.setFood()
        })
    }

    setFood() {
        let food = []
        for(let i = 0; i < 3; i++) {
            let x = Math.floor(Math.random() * 13 + 1)
            let y = Math.floor(Math.random() * 13 + 1)
            this.state.snake.map(cell => {
                if (x == cell.x && y == cell.y) {
                    x = Math.floor(Math.random() * 13 + 1)
                    y = Math.floor(Math.random() * 13 + 1)
                }
            })
            food.push({
                x,
                y,
                word: this.state.currentWordGroup.words[i]
            })
        }
        for (let i = 0; i < 3; i++) {
            for (let j = 1; i < 3; i++) {
                if (food[i].x == food[j].x && food[i].y == food[j].y) {
                    food[i].x = Math.floor(Math.random() * 13 + 1)
                    food[i].y = Math.floor(Math.random() * 13 + 1)
                }
            }
        }
        let grid = this.state.grid.slice(0)
        food.map((item, i) => {
            grid[item.y].props.children[item.x] = (
                <View key={i * 912837 + 1} style={[
                    this.state.eaten.includes(item.word.value) && styles.cellStyles,
                    this.state.correct.includes(item.word.value) && styles.foodCorrect,
                    !this.state.eaten.includes(item.word.value) && !this.state.correct.includes(item.word.value) && styles[`food${i}`],
                    i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                </View>
            )

        })
        // console.log('Needed: ', this.state.currentWordGroup.trueWord)
        this.setState({ food, grid, correct: [], eaten: [] }, () => {
            // console.log('Food: ', this.state.food)
        })
    }

    eatFood(pos) {
        if(pos == undefined) {
            clearInterval(this.state.tick)
            return
        }
        let eaten
        this.state.food.map((item, index) => {
            if(item.x ==  pos.x && item.y ==  pos.y) {
                eaten = item
            }
        })
        if(eaten) {
            console.log('Food: ', this.state.food)
            console.log('Needed: ', this.state.currentWordGroup.trueWord)
            console.log('word:', eaten)
            if (eaten.word.value == this.state.currentWordGroup.trueWord) {
                console.log('Correct, ', eaten)
                let correct = this.state.correct.slice(0)
                correct.push(eaten.word.value)
                this.setState({ correct }, () => {
                    this.changeCurrentWordGroup()
                })
            } else {
                let eatenWords = this.state.eaten.slice(0)
                eatenWords.push(eaten.word.value)
                this.setState({ eaten: eatenWords })
            }
        }
    }

    moveSnake() {
        let newSnake = this.state.snake.slice(0)
        newSnake.pop()
        newSnake.unshift({
            x: newSnake[0].x + this.state.direction[1],
            y: newSnake[0].y + this.state.direction[0]
        })
        let snakeCoords = newSnake.slice(0)
        if (newSnake[0].x > this.state.grid.length - 1  || newSnake[0].x < 0 || newSnake[0].y > this.state.grid.length - 1  || newSnake[0].y < 0) {
            clearInterval(this.state.tick)
            this.setState({ dead: true, food: [], tick: null })
            return
        }
        snakeCoords.map((coord, i) => {
            if (i != 0 && newSnake[0].x == coord.x && newSnake[0].y == coord.y) {
                clearInterval(this.state.tick)
                this.setState({ dead: true, food: [],  eaten: [], correct: [], currentWordGroup: this.state.wordGroups[0], tick: null })
                return
            }
        })
        grid = this.setGrid()
        newSnake.map((cell, i) => {
            grid[cell.y].props.children[cell.x] = (
                <View key={(i + 1) * 952} style={[styles.snake, i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                </View>
            )
        })

        if (newSnake[0].x != 5 && newSnake[0].y != 0) {
            this.eatFood({
                x: newSnake[0].x,
                y: newSnake[0].y
            })
        }

        this.setState({ snake: newSnake, grid })
    }

    setGrid() {
        let grid = []
        let food = this.state.food.slice(0)
        this.state.emptyGrid.map((item, index) => {
            let cells = item.map((cell, i) => {
                if (food[0] && food[0].x == i && food[0].y == index) {
                    return (
                        <View key={i * 1000 } style={[
                            this.state.correct.includes(food[0].word.value) && styles.foodCorrect,
                            !this.state.eaten.includes(food[0].word.value) && !this.state.correct.includes(food[0].word.value) && styles['food0'],
                            this.state.eaten.includes(food[0].word.value) && styles.cellStyles,
                            i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                        </View>
                    )
                } else if (food[1] && food[1].x == i && food[1].y == index) {
                    return (
                        <View key={i * 1000 } style={[
                            this.state.correct.includes(food[1].word.value) && styles.foodCorrect,
                            !this.state.eaten.includes(food[1].word.value) && !this.state.correct.includes(food[1].word.value) && styles['food1'],
                            this.state.eaten.includes(food[1].word.value) && styles.cellStyles,
                            i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                        </View>
                    )
                } else if (food[2] && food[2].x == i && food[2].y == index) {
                    return (
                        <View key={i * 1000 } style={[
                            this.state.correct.includes(food[2].word.value) && styles.foodCorrect,
                            !this.state.eaten.includes(food[2].word.value) && !this.state.correct.includes(food[2].word.value) && styles['food2'],
                            this.state.eaten.includes(food[2].word.value) && styles.cellStyles,
                            i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                        </View>
                    )
                } else {
                    return (
                        <View key={i * 1000} style={[styles.cellStyles, i == this.state.grid.length - 1 && { marginRight: 0 }]}>
                        </View>
                    )
                }
        })
            let row = <View key={index} style={styles.rowStyles}>{cells}</View>
            grid.push(row)
        })
        return grid
    }

    restart() {
        clearInterval(this.state.tick)
        this.createSnake()
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {},
            onPanResponderMove: (evt, gestureState) => {
                // console.log('gestureState.moveX: ', gestureState.moveX)
                // console.log('lastCoords.x + 25: ', lastCoords.x + 25)
                // console.log('gestureState.moveX: ', gestureState.moveY)
                // console.log('lastCoords.y + 25: ', lastCoords.y + 25)
                if (gestureState.moveX > (lastCoords.x + 1) && gestureState.moveY >= lastCoords.y - 0.05 && gestureState.moveY <= lastCoords.y + 0.05) {
                    if (this.state.direction[1] != -1)
                        this.setState({ direction: [0, 1] })
                } else if ((gestureState.moveY + 0.5) < lastCoords.y && gestureState.moveX >= lastCoords.x - 0.1 && gestureState.moveX <= lastCoords.x + 0.1) {
                    if (this.state.direction[0] != 1)
                        this.setState({ direction: [-1, 0] })
                } else if ((gestureState.moveX + 1) < lastCoords.x && gestureState.moveY >= lastCoords.y - 0.05 && gestureState.moveY <= lastCoords.y + 0.05) {

                    if (this.state.direction[1] != 1)
                        this.setState({ direction: [0, -1] })
                } else if (gestureState.moveY > (lastCoords.y + 0.5) && gestureState.moveX >= lastCoords.x - 0.1 && gestureState.moveX <= lastCoords.x + 0.1) {

                    if (this.state.direction[0] != -1)
                        this.setState({ direction: [1, 0] })
                }
                lastCoords.x = gestureState.moveX
                lastCoords.y = gestureState.moveY
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {},
            onPanResponderTerminate: (evt, gestureState) => {},
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return true;
            },
        })
    }

    componentDidMount() {
        this.setState({ totalAttempts: this.props.navigation.state.params.testSingle.attempts.snake })
        let words = this.props.navigation.state.params.testSingle.tasks[0].content
        this.setWordGroups(words)
        this.restart()
    }

    static navigationOptions = {
        title: 'Змейка'
    }

    render() {
        const {
            main,
            gameWrapper,
            snakeContainer,
            gridStyles,
            rowStyles,
            cellStyles,
            wordsContainer,
            wordKey,
            translation,
            wordContainer,
            keyContainer,
        } = styles

        return (
            <View style={main}>
                <Text style={{ fontSize: 18, marginBottom: 30, fontWeight: 'bold' }}>Пройдено раз: {this.state.doneAttempts}/{this.state.totalAttempts}</Text>
                {
                this.state.dead && (
                    <View style={{ flexDirection: 'column', alignItems: 'center', width: '80%' }}>
                        <Text style={{ marginBottom: 10 }}>Вы проиграли :(</Text>
                        <Button
                            blue
                            title="Перезапуск"
                            width="100%"
                            handler={this.restart}
                        />
                    </View>
                    )
                }
                {
                    this.state.win && (
                        <View style={{ flexDirection: 'column', alignItems: 'center', width: '80%' }}>
                            <Text style={{ marginBottom: 10 }}>Победа!</Text>
                            <Button
                                blue
                                title="Перезапуск"
                                width="100%"
                                handler={this.restart}
                            />
                        </View>
                    )
                }
                {
                !this.state.dead && !this.state.win && (
                    <View style={gameWrapper} {...this._panResponder.panHandlers}>
                        <View style={snakeContainer}>
                            <View style={gridStyles}>
                                {this.state.grid}
                            </View>
                        </View>
                        <View style={wordsContainer}>
                            <View style={keyContainer}>
                                <Text style={wordKey}>{this.state.currentWordGroup.key}</Text>
                            </View>
                            {
                                this.state.currentWordGroup.words.length == 3 && (
                                        <View style={[
                                            wordContainer, { backgroundColor: '#1E7A41' },
                                            this.state.eaten.includes(this.state.currentWordGroup.words[0].value) && { backgroundColor: '#CE4257' },
                                            this.state.correct.includes(this.state.currentWordGroup.words[0].value) && { backgroundColor: '#5DB7DE' }
                                        ]}>
                                            <Text style={[
                                                translation,
                                                this.state.eaten.includes(this.state.currentWordGroup.words[0].value) && { textDecorationLine: 'line-through' }
                                            ]}>
                                                {this.state.currentWordGroup.words[0] ? this.state.currentWordGroup.words[0].value : ''}
                                            </Text>
                                        </View>
                                )
                            }
                            {
                                this.state.currentWordGroup.words.length == 3 && (
                                        <View style={[
                                            wordContainer, { backgroundColor: '#3E8BDA' },
                                            this.state.eaten.includes(this.state.currentWordGroup.words[1].value) && { backgroundColor: '#CE4257' },
                                            this.state.correct.includes(this.state.currentWordGroup.words[1].value) && { backgroundColor: '#5DB7DE' }
                                        ]}>
                                            <Text style={[
                                                translation,
                                                this.state.eaten.includes(this.state.currentWordGroup.words[1].value) && { textDecorationLine: 'line-through' }
                                            ]}>
                                                {this.state.currentWordGroup.words[1] ? this.state.currentWordGroup.words[1].value : ''}
                                            </Text>
                                        </View>
                                )
                            }
                            {
                                this.state.currentWordGroup.words.length == 3 && (
                                        <View style={[
                                            wordContainer, { backgroundColor: '#F2C63D' },
                                            this.state.eaten.includes(this.state.currentWordGroup.words[2].value) && { backgroundColor: '#CE4257' },
                                            this.state.correct.includes(this.state.currentWordGroup.words[2].value) && { backgroundColor: '#5DB7DE' }
                                        ]}>
                                            <Text style={[
                                                translation,
                                                this.state.eaten.includes(this.state.currentWordGroup.words[2].value) && { textDecorationLine: 'line-through' }
                                            ]}>
                                                {this.state.currentWordGroup.words[2] ? this.state.currentWordGroup.words[2].value : ''}
                                            </Text>
                                        </View>
                                )
                            }
                        </View>
                    </View>
                    )
                }
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 30
    },
    gameWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        flex: 1,
        padding: 0
    },
    wordsContainer: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginBottom: 30,
        flexWrap: 'wrap'
    },
    wordContainer: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 5,
        paddingVertical: 10
    },
    keyContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20
    },
    wordKey: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    translation: {
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold'
    },
    snakeContainer: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginBottom: 30,
        flexWrap: 'wrap'
    },
    gridStyles: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 300
    },
    rowStyles: {
        flexDirection: 'row',
        flex: 1,
        marginBottom: 1,
        height: 15
    },
    cellStyles: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        backgroundColor: '#D3DFF6',
        borderRadius: 2
    },
    snake: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        backgroundColor: '#3D3F3E',
        borderRadius: 2
    },
    food0: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        borderRadius: 2,
        backgroundColor: '#1E7A41'
    },
    food1: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        borderRadius: 2,
        backgroundColor: '#3E8BDA'
    },
    food2: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        borderRadius: 2,
        backgroundColor: '#F2C63D'
    },
    foodCorrect: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        borderRadius: 2,
        backgroundColor: '#5DB7DE'
    },
    foodIncorrect: {
        width: Math.floor(Dimensions.get('window').width / 20),
        marginRight: 2,
        height: Math.floor(Dimensions.get('window').width / 20),
        borderRadius: 2,
        backgroundColor: '#CE4257'
    }
})