import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableHighlight, SectionList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import randomize from 'randomatic'
import _ from 'lodash'

import Button from '../../../components/Button'

export default class Matching extends Component {
    constructor() {
        super()
        this.state = {
            words: [],
            render: [],
            firstNumber: '',
            firstLink: null,
            secondNumber: '',
            secondLink: null,
            correct: [],
            incorrect: [],
            totalAttempts: 0,
            doneAttempts: 0,
            done: false,
            percentage: 0,
            error: 0,
            msg: ''
        }
        this.prepareRender = this.prepareRender.bind(this)
        this.checkConditions = this.checkConditions.bind(this)
        this.restart = this.restart.bind(this)
    }

    static navigationOptions = {
        title: 'Найди пару'
    }
    
    restart() {
        this.setState({ words: _.shuffle(this.state.words), firstNumber: '', firstLink: null, 
            secondNumber: '', secondLink: null, done: false, correct: [], incorrect: [],
            percentage: 0
         }, () => {
             this.prepareRender()
         })
    }

    prepareRender() {
        let render = []
        this.state.words.map((word, index) => {
            let keyIndex = randomize('0', 10)
            let key = {
                val: word.key,
                id: keyIndex,
                index
            }

            let defIndex = randomize('0', 10)
            let def = {
                val: word.value,
                id: defIndex,
                index
            }

            render.push(key)
            render.push(def)
        })
        render = _.shuffle(render)
        this.setState({ render })
    }

    componentDidMount() {
        let words = _.shuffle(this.props.navigation.state.params.testSingle.tasks[0].content)
        this.setState({ totalAttempts: this.props.navigation.state.params.testSingle.attempts.matching, words }, () => {
            this.prepareRender()
        })
    }

    checkConditions() {
        let length = this.state.incorrect.length + this.state.correct.length
        if(length == this.state.words.length) {
            let percentage = Math.round(100 - this.state.error * 100 / this.state.words.length)
            if(percentage < 0)
                percentage = 0
            if (percentage >= 90) {
                if (percentage == 100) {
                    this.setState({ msg: 'Отлично!', done: true, doneAttempts: this.state.doneAttempts + 1, percentage })
                } else {
                    this.setState({ msg: 'Очень хорошо!', done: true, doneAttempts: this.state.doneAttempts + 1, percentage })
                }
            } else if (percentage < 90 && percentage >= 60) {
                this.setState({ msg: 'Хорошо, но необходимо пройти задание повторно!', done: true, percentage })
            } else if (percentage < 60) {
                this.setState({ msg: 'Пройди задание повторно!', done: true, percentage })
            }
        }
    }

    select(word) {
        if(!this.state.correct.includes(word.link)
            && this.state.firstNumber != word.number
            && this.state.secondNumber != word.number) {
            if (this.state.firstLink != null) {
                this.setState({
                    secondNumber: word.number,
                    secondLink: word.link,
                    incorrect: []
                }, () => {
                    if (this.state.firstLink == this.state.secondLink) {
                        let correct = Object.assign([], this.state.correct)
                        correct.push(word.link)
                        this.setState({
                            firstNumber: '',
                            firstLink: null,
                            secondNumber: '',
                            secondLink: null,
                            correct
                        }, () => {
                            this.checkConditions()
                        })
                    } else {
                        let incorrect = Object.assign([], this.state.incorrect)
                        incorrect.push(this.state.firstNumber)
                        incorrect.push(this.state.secondNumber)
                        this.setState({
                            firstNumber: '',
                            firstLink: null,
                            secondNumber: '',
                            secondLink: null,
                            error: this.state.error + 1,
                            incorrect
                        })
                    }
                })
                return
            }
            this.setState({ firstNumber: word.number, firstLink: word.link, incorrect: [] })
        }
    }

    render() {
        const {
            main,
            gameWrapper,
            wordsContainer,
            wordContainer,
            selectedWord,
            correctWordPair,
            wrongWordPair,
            marginBottom
        } = styles

        return (
            <ScrollView contentContainerStyle={main}>
                <View style={gameWrapper}>
                    <Text style={{ fontSize: 18, marginBottom: 30, fontWeight: 'bold' }}>Пройдено раз: {this.state.doneAttempts}/{this.state.totalAttempts}</Text>
                    {
                        !this.state.done && (
                            <View style={wordsContainer}>
                                {
                                    this.state.render.map(word => (
                                        <TouchableHighlight
                                            key={randomize('0', 20)}
                                            disabled={this.state.correct.includes(word.index) ||
                                                this.state.firstNumber == word.id ||
                                                this.state.secondNumber == word.id}
                                            underlayColor="rgb(139, 151, 225)"
                                            style={[styles.wordContainer,
                                            this.state.words.length > 3 && styles.marginBottom,
                                            this.state.firstNumber == word.id && styles.selectedWord,
                                            this.state.secondNumber == word.id && styles.selectedWord,
                                            this.state.incorrect.includes(word.id) && styles.wrongWordPair,
                                            this.state.correct.includes(word.index) && styles.correctWordPair]}
                                            onPress={() => this.select({ number: word.id, link: word.index })}>
                                            <Text>{word.val}</Text>
                                        </TouchableHighlight>
                                    ))
                                }
                            </View>
                        )
                    }
                    {
                        this.state.done && (
                            <View style={{ flexDirection: 'column', alignItems: 'center', width: '80%' }}>
                                <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 10 }}>{this.state.msg}</Text>
                                <Text style={{ fontSize: 14, marginBottom: 10 }}>Ваш результат: {this.state.percentage}%</Text>
                                <Button
                                    blue
                                    title="Перезапуск"
                                    width="100%"
                                    handler={this.restart}
                                />
                            </View>
                        )
                    }
                </View>
            </ScrollView>

        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 30,
        overflow: 'hidden',
        minHeight: 1,
    },
    gameWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        padding: 0
    },
    wordsContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginBottom: 30,
        flexWrap: 'wrap'
    },
    wordContainer: {
        flexDirection: 'row',
        backgroundColor: '#BCE7FD',
        justifyContent: 'center',
        width: '30%',
        overflow: 'hidden',
        minHeight: 1,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: '1%',
        paddingRight: '1%',
        borderRadius: 5,
    },
    selectedWord: {
        backgroundColor: 'rgb(139, 151, 225)'
    },
    correctWordPair: {
        backgroundColor: '#7EB77F'
    },
    wrongWordPair: {
        backgroundColor: '#CE4257'
    },
    word: {
        fontSize: 16,
    },
    marginBottom: {
        marginBottom: 10
    }
})