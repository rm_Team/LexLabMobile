import React, { Component } from 'react'
import { Text, View } from 'react-native'
import SortableList from 'react-native-sortable-list'
import EStyleSheet from 'react-native-extended-stylesheet'
import shuffle from 'shuffle-array'

import Button from '../../../components/Button'
import Row from '../../../common/Row'

export default class Scramble extends Component {
    constructor() {
        super()
        this.state = {
            success: false,
            keyWord: 'Перевод',
            neededWord: 'Translate',
            splittedWord: {},
            otalAttempts: 0,
            doneAttempts: 0,
        }
        this.onChangeOrder = this.onChangeOrder.bind(this)
        this.shuffleWord = this.shuffleWord.bind(this)
        this.nextWord = this.nextWord.bind(this)
    }

    static navigationOptions = {
        title: 'Скрэмбл'
    }

    renderRow = ({ data, active }) => {
        return <Row data={data} active={active} />
    }

    onChangeOrder(nextOrder) {
        let word = ''
        nextOrder.map((item, index) => {
            word += this.state.splittedWord[item].letter
        })
        if (word == this.state.neededWord) {
            this.setState({ success: true })
        }
    }

    nextWord() {
        this.setState({ keyWord: 'Транскрипция', neededWord: 'Transcription', success: false }, () => {
            this.shuffleWord()
        })
    }

    shuffleWord() {
        let splitted = this.state.neededWord
        splitted = splitted.split('')
        splitted = shuffle(splitted)
        let splittedWord = {}
        splitted.map((letter, index) => splittedWord[index] = { letter })
        this.setState({ splittedWord })
    }

    componentWillMount() {
        this.shuffleWord()
    }

    componentDidMount() {
        this.setState({ totalAttempts: this.props.navigation.state.params.testSingle.attempts.scramble })
    }

    render() {
        const {
            main,
            gameWrapper,
            gameContainer,
            definition,
            list,
            container,
            correct,
            neededWord
        } = styles

        return (
            <View style={main}>
                <View style={gameWrapper}>
                    <Text style={{ fontSize: 18, marginBottom: 30, fontWeight: 'bold' }}>Пройдено раз: 0/1</Text>
                    <View style={[gameContainer, this.state.success && { paddingLeft: 0, paddingRight: 0, paddingBottom: 0, width: '80%' }]}>
                        <Text style={definition}>{this.state.keyWord}</Text>
                        {
                            !this.state.success ? (
                                <SortableList
                                    horizontal
                                    style={list}
                                    data={this.state.splittedWord}
                                    renderRow={this.renderRow}
                                    onChangeOrder={this.onChangeOrder}
                                />
                            ) :
                                <View style={correct}>
                                    <Text style={neededWord}>{this.state.neededWord}</Text>
                                </View>
                        }
                    </View>
                    {
                        this.state.success && (
                            <Button
                                blue
                                width="80%"
                                padding={20}
                                fontSize={18}
                                handler={this.nextWord}
                                title="Следующее слово"
                            />
                        )
                    }
                </View>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 30
    },
    gameWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        padding: 0
    },
    gameContainer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 20,
        paddingBottom: 0,
        marginBottom: 30,
        borderRadius: 5
    },
    definition: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20
    },
    list: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 60,
        width: '100%',
        padding: 0
    },
    correct: {
        width: '100%',
        padding: 20,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '$success'
    },
    neededWord: {
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white'
    }
})
