import React, { Component } from 'react'
import { Text, View } from 'react-native'
import LineLink from '../../components/LineLink'

export default class Learn extends Component {
	static navigationOptions = {
		tabBarLabel: 'УЧИТЬ'
	}

	// componentWillMount() {
	// 	console.log(this.props)
	// }

	// Все данные для определенное теста хранятся в this.props.navigation.state.params.test
	// Далее уже их можно использовать, как пример вывел id урока
	

	render() {
		const { navigation } = this.props
		return (
			<View>
				<LineLink
					title="Выучи слова"
					onPress={() => {
						navigation.navigate('Flashcards', {
							testSingle: navigation.state.params.test
						})
					}}
					done
					attempts="0/1"
				/>
				<LineLink
					title="Найди пару"
					onPress={() => {
						navigation.navigate('Matching', {
							testSingle: navigation.state.params.test
						})
					}}
					done
					attempts="0/1"
				/>
				<LineLink
					title="Впиши слово"
					onPress={() => {
						navigation.navigate('Typein', {
							testSingle: navigation.state.params.test
						})
					}}
					done
					attempts="0/1"
				/>
			</View>
		)
	}
}