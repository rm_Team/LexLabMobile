import React, { Component } from 'react'
import { Text, View } from 'react-native'
import LineLink from '../../components/LineLink'

export default class Learn extends Component {
  static navigationOptions = {
    tabBarLabel: 'ИГРАТЬ'
  }

  render() {
    const { navigation } = this.props

    return (
      <View>
        <LineLink
          title="Скрэмбл"
          onPress={() => {
						navigation.navigate('Scramble', {
							testSingle: navigation.state.params.test
						})
					}}
          done
          percentage="0/1"
        />
        <LineLink
          title="Змейка"
          onPress={() => {
						navigation.navigate('Snake', {
							testSingle: navigation.state.params.test
						})
					}}
          done
          percentage="0/1"
        />
      </View>
    )
  }
}