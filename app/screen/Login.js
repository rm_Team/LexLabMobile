import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, TextInput, AsyncStorage, ActivityIndicator } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { Ionicons } from '@expo/vector-icons'
import Expo from 'expo'
import axios from 'axios'
import { connect, getState } from 'react-redux'
import { loginUser } from './../actions'
import configureStore from './../config/configureStore'

const store = configureStore()

import Input from '../components/Input'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      passActive: false,
      login: '',
      password: '',
      errorMessage: '',
      error: {
        login: false,
        password: false,
        jwt: false
      },
      wrong: {
        login: false,
        password: false
      },
      loading: false
    }

    this.handler = this.handler.bind(this)
    this.submit = this.submit.bind(this)
  }

  static navigationOptions = {
    title: "Вход",
    headerLeft: null,
    header: null
  }
  
  componentWillUpdate() {
    store.subscribe(() => {
      this.setState({ loading: false })
      if(!store.getState().AuthReducer.loginError) {
        this.props.screenProps[0]()
      } else {
        this.setState({ wrong: { login: true, password: false } })
        // Нужно здесь продумать обработку нормальную и переделать
      }
    })

  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      RobotoRegular: require("../../assets/fonts/Roboto/Roboto-Bold.ttf")
    })
    this.setState({ isReady: true })
  }

  handler(text, property) {
    this.setState({ [property]: text })
  }

  submit() {
    const { login, password } = this.state
    this.state.error = {
      login: false,
      password: false,
      jwt: false
    }
    this.setState({ errorMessage: '', loading: true })
    if (login.trim().length > 0 && password.length > 0) {
      this.setState({ loading: true})
      store.dispatch(loginUser({ login, password }))
    }
    if (login.trim().length == 0) {
      let newError = this.state.error
      newError.login = true
      this.setState({ error: newError, loading: false })
    }
    if (password.length == 0) {
      let newError = Object.assign({}, this.state.error)
      newError.password = true
      this.setState({ error: newError, loading: false })
    }
  }

  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />
    }

    const {
      back,
      inputsWrapper,
      inputs,
      main,
      h1,
      regPageButton,
      textCenter,
      containerNoBorder,
      inputWithIcon,
      errorText,
      backWithError,
      recover,
      textRight
    } = style

    const { navigate } = this.props.navigation

    var error;

    if(this.state.errorMessage) {
      error = <Text style={errorText}>{ this.state.errorMessage }</Text>
    }

    return (
      <ScrollView contentContainerStyle={main}>
        <View style={back}>
          {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
          <Text>{'<––   '}</Text>
        </TouchableOpacity> */}
          <Text style={h1}>Вход</Text>
        </View>
        {
          this.state.wrong.login && (
            <View style={backWithError}>
              <Text style={errorText}>Неверный логин</Text>
            </View>
          )
        }
        {
          this.state.wrong.password && (
            <View style={backWithError}>
              <Text>Неверный пароль</Text>
            </View>
          )
        }
        <View style={inputsWrapper}>
          <View style={inputs}>
            <Input
              error={this.state.error.login ? true : false}
              handler={this.handler}
              property="login"
              placeholder="Логин"
            />
            <View style={containerNoBorder}>
              <TextInput
                secureTextEntry={this.state.passActive ? false : true}
                style={inputWithIcon}
                placeholderTextColor={this.state.error.password ? '#CE4257' : 'lightgray'}
                underlineColorAndroid='transparent'
                onChangeText={text => this.setState({ password: text })}
                placeholder="Пароль"
              />
              <TouchableOpacity onPress={() => this.setState({ passActive: !this.state.passActive })}>
                <Ionicons
                  name={this.state.passActive ? 'md-eye' : 'md-eye-off'}
                  size={14}
                  color={this.state.error.password ? '#CE4257' : 'lightgray'}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={recover}>
            <TouchableOpacity onPress={() => navigate('Remember')}>
              <Text style={textRight}>Забыли пароль?</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={regPageButton}
            onPress={() => this.submit()}
          >
            {this.state.loading ?
              <ActivityIndicator color="white" /> :
              <Text style={textCenter}>ВОЙТИ</Text>}
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const style = EStyleSheet.create({
  back: {
    padding: 10,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  recover: {
    marginTop: 10,
    paddingRight: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 250
  },
  backWithError: {
    marginBottom: 14,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputsWrapper: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  inputs: {
    paddingLeft: 20,
    paddingRight: 20,
    width: 250,
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 10,
    borderColor: 'rgb(220, 220, 220)'
  },
  main: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  h1: {
    fontSize: 36,
    fontWeight: 'bold'
  },
  regPageButton: {
    width: 250,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    marginTop: 35,
    backgroundColor: 'rgb(139, 151, 225)',
  },
  textCenter: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    
  },
  textRight: {
    textAlign: 'right',
    color: '#A0A5A4',
    fontWeight: '400',
    
  },
  containerNoBorder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    width: '100%',
    paddingLeft: 5,
    paddingRight: 5,
  },
  containerNoBorderError: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    width: '100%',
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '$error'
  },
  input: {
    height: 40,
    fontSize: 14
  },
  inputWithIcon: {
    width: 175,
    height: 40,
    fontSize: 14
  },
  errorText: {
    color: '$error'
  }
})

const mapStateToProps = state => {
  return {
    login: state.login,
    password: state.password,
    token: state.token,
    loginError: state.loginError
  }
}

export default connect(mapStateToProps, {
  loginUser
})(Login)