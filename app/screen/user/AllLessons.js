import React, { Component } from 'react'
import { ListView, Text, View, TouchableHighlight, AsyncStorage, ActivityIndicator } from 'react-native'
import { connect, getState } from 'react-redux'
import { checkLoginState, fetchUserInfo } from '../../actions'

import configureStore from '../../config/configureStore'
import LineLink from '../../components/LineLink'

const store = configureStore()

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
})

class AllLessons extends Component {
  constructor() {
    super()

    this.state = {
      loading: true,
      updated: false,
      dataSource: ds.cloneWithRows([]),
      selectGroup: {},
      groups: [],
      tests: [],
      username: ''
    }
  }

  async componentWillMount() {
    const jwt = await AsyncStorage.getItem('@easylexlab:jwt')
    store.dispatch(fetchUserInfo(jwt))
  }

  async componentDidMount() {
    store.subscribe(() => {
      this.setState({
        groups: store.getState().AuthReducer.user._groups,
        username: store.getState().AuthReducer.user.username
      })
    })
    
  }

  async componentWillUpdate() {

      const groupStorage = await AsyncStorage.getItem('@easylexlab:group')

      let dataSource = [], tests = []

      for(let group of this.state.groups) {
        if (group.name == groupStorage) {
          for (let test of group._tests) {
            test.modalOpen = false
            var done = false
            var done = false
            if (test.results) {
              for (let result of test.results) {
                if (result.username == this.state.username) {
                  done = true
                }
              }
              if (done) tests.push(test)
            }
          }
          
        }   
      }
      dataSource = ds.cloneWithRows(tests)
      this.setState({ dataSource, tests, updated: true, loading: false })

  }

  _renderRow = test => {
    const { navigation } = this.props

    return (
      <LineLink title={test.name} onPress={() => {
        navigation.navigate('Test', { test: test })
      }}/>
    )
  }

  render() {
    if(this.state.tests.length > 0) {
      return (
        <View style={{ flexDirection: 'column', flex: 1 }}>
          <ListView style={{ flexDirection: 'column', flex: 1 }}
            dataSource={this.state.dataSource}
            renderRow={dataRow => this._renderRow(dataRow)}
          />
        </View>
      )
    } else {
      return (
        <View style={[
          { flexDirection: 'column', flex: 1, justifyContent: 'center' },
          !this.state.loading && { justifyContent: 'flex-start', paddingTop: 30 }
          ]}>
          {
            this.state.loading ?
              <ActivityIndicator color="black" />
              :
              <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
                <Text>Выполненных заданий нет.</Text>
              </View>
          }
        </View>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    currentGroup: state.currentGroup,
    groups: state.groups
  }
}

export default connect(
  mapStateToProps,
  { checkLoginState, fetchUserInfo }
)(AllLessons)