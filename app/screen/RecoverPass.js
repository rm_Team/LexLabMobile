import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Input from '../components/Input'

export default class RecoverPass extends Component {
    constructor() {
        super()
        this.state = {
            isReady: false,
            email: ''
        }
        this.handler = this.handler.bind(this)
    }

    static navigationOptions = {
        title: "Восстановление пароля",
        headerLeft: null,
        header: null
    }

    async componentWillMount() {
        await Expo.Font.loadAsync({
            RobotoRegular: require("../../assets/fonts/Roboto/Roboto-Bold.ttf")
        })
        this.setState({ isReady: true })
    }

    handler(text, property) {
        this.setState({ [property]: text })
    }

    render() {
        const {
            main, h1, titleWrapper, inputWrapper, input, reсPageButton,
            textCenter
         } = style

        if(!this.state.isReady) {
            return <Expo.AppLoading />
        }

        return (
            <View style={main}>
                <View style={titleWrapper}>
                    <Text style={h1}>Восстановление пароля</Text>
                </View>
                <View style={inputWrapper}>
                    <View style={input}>
                        <Input
                            email
                            handler={this.handler}
                            property="email"
                            placeholder="Email"
                            last
                        />
                    </View>
                    <TouchableOpacity
                        style={reсPageButton}
                        onPress={() => this.submit()}
                    >
                        <Text style={textCenter}>ВОССТАНОВИТЬ</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const style = EStyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    titleWrapper: {
        padding: 10,
        marginBottom: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputWrapper: {
        marginTop: 20,
        flexDirection: 'column',
        alignItems: 'center'
    },
    input: {
        paddingLeft: 20,
        paddingRight: 20,
        width: 250,
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 10,
        borderColor: 'rgb(220, 220, 220)'
    },
    reсPageButton: {
        width: 250,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        marginTop: 35,
        backgroundColor: 'rgb(139, 151, 225)',
    },
    h1: {
        fontSize: 26,
        fontFamily: 'RobotoRegular'
    },
    textCenter: {
        textAlign: 'center',
        color: 'white',
        fontWeight: '400',
        fontFamily: 'RobotoRegular'
    },
})