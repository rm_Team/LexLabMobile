import React, { Component } from 'react';
import { View, Text } from 'react-native'

class Result extends Component {
  constructor() {
    super()
    this.state = {
        isReady: false
    }
  }

  async componentWillMount() {
      await Expo.Font.loadAsync({
          RobotoRegular: require("../../assets/fonts/Roboto/Roboto-Bold.ttf")
      })
      this.setState({ isReady: true })
  }

  static navigationOptions = {
    title: 'Результаты',
    headerLeft: null,
    headerTintColor: '#3F3F3F',
    headerTitleStyle: {
        fontWeight: '400',
        fontSize: 18
    },
    headerStyle: {
        backgroundColor: '#FAFAFA',
        shadowOffset: {
            height: 0
        },
        shadowOpacity: 0,
        shadowRadius: 0,
        elevation: 0,
    }
  }

  render() {
      if (!this.state.isReady) {
          return <Expo.AppLoading />
      }

      return (
          <View style={{flex: 1}}>
              <Text>Results</Text>
          </View>
      );
  }
}

export default Result;