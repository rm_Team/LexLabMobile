import React, { Component } from 'react'
import { Text, ScrollView, View, AsyncStorage } from 'react-native'
import Expo from 'expo'
import { connect, getState } from 'react-redux'
import { fetchUserInfo } from '../actions'
import configureStore from '../config/configureStore'

const store = configureStore()

import Message from './../components/Message'

class Messages extends Component {
    constructor() {
        super()
        this.state = {
            isReady: false,
            groups: [],
            selectGroup: {}
        }
    }

    async componentWillMount() {
        const jwt = await AsyncStorage.getItem('@easylexlab:jwt')
        store.dispatch(fetchUserInfo(jwt))
        await Expo.Font.loadAsync({
            RobotoRegular: require("../../assets/fonts/Roboto/Roboto-Bold.ttf")
        })
        this.setState({ isReady: true })
    }

    componentDidMount() {
        store.subscribe(() => {
          this.setState({
            groups: store.getState().AuthReducer.user._groups
          })
        })
      }

    async componentWillUpdate() {
        const groupStorage = await AsyncStorage.getItem('@easylexlab:group')
        this.setState({
          selectGroup: {
            name: groupStorage,
            id: ''
          }
        })
    }

    render() {
        if (!this.state.isReady) {
            return <Expo.AppLoading />
        }

        const { groups, selectGroup } = this.state

        return (
            <ScrollView style={{flex: 1}}>
                <View style={{
                    width: '100%',
                    paddingVertical: 8,
                    backgroundColor: '#F2f2f2',
                    borderBottomWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 15,
                    borderColor: '#f0f0f0'
                }}>
                    <Text>Ваша группа: {this.state.selectGroup.name}</Text>
                </View>
                <View>
                {groups.map((item, index) => {
                    if (item.name == selectGroup.name) {
                        return item.messages.map((messageSingle, indexSingle) => (
                            <Message key={indexSingle} text={messageSingle.text} author={messageSingle.author} />
                        ))
                    }
                })}
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = state => ({
    groups: state.groups
})

export default connect(
    mapStateToProps,
    { fetchUserInfo }
  )(Messages)