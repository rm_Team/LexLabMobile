import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Expo from 'expo'

class Home extends Component {
  static navigationOptions = {
    title: 'Главная',
    headerLeft: null,
    header: null
  }

  render() {
    const {
      homeContainer,
      center,
      h1,
      buttons,
      loginButton,
      textButton
    } = styles

    const { navigate } = this.props.navigation

    return (
      <View style={homeContainer}>
        <View style={center}>
          <Text style={h1}>EasyLexLab</Text>
        </View>
        <View style={buttons}>
          <TouchableOpacity
            onPress={() => navigate('Login')}
            style={loginButton}
          >
            <Text style={textButton}>ВХОД</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  homeContainer: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection: "column",
  },
  center: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    flex: 2,
  },
  h1: {
    fontSize: 36,
    fontWeight: 'bold',
    marginTop: 60
  },
  buttons: {
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 2,
  },
  loginButton: {
    borderRadius: 10,
    paddingTop: 10,
    paddingBottom: 10,
    width: 250,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(139, 151, 225)",
    bottom: 45
  },
  textButton: {
    color: "#FFF",
    fontSize: 14,
    fontWeight: 'bold'
  }
})

export default Home
