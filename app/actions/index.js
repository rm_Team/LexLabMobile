import axios from 'axios'
import {
    FETCHING_USER,
    FETCHING_AVATAR,
    UPDATE_USER,
    UPDATE_AVATAR,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_STATE,
    LOGOUT_USER,
    LOGIN_USER_ERROR,
    FETCH_USER_INFO
} from '../types'
import { AsyncStorage } from 'react-native'

export const loginUser = ({ login, password }) => {
    console.log('Login and pass to send: ', login, password)
    return (dispatch, getState) => {
        axios.post('http://ealapi.tw1.ru/api/login', {
            username: login,
            password: password
        }).then(res => {
            if(!res.data.error) {
                AsyncStorage.setItem('@easylexlab:jwt', res.data.token).then(e => {
                    axios.post('http://ealapi.tw1.ru/api/user', {}, {
                        headers: {
                            'Content-type': 'application/json',
                            'Authorization': 'Bearer ' + res.data.token
                        }
                    }).then(response => {
                        let completedTasks = []
                        let uncompletedTasks = []
                        for (var test of response.data.user._groups[0]._tests) {
                            var done = false
                            if (test.results) {
                                for (let result of test.results) {
                                    if (result.username == response.data.user.username) {
                                        done = true
                                    }
                                }
                                if (!done) {
                                    uncompletedTasks.push(test)
                                } else {
                                    completedTasks.push(test)
                                }
                            }
                        }
                        if (response.data.user.picUrl != null && response.data.user.picUrl != undefined) {
                            axios.post('http://ealapi.tw1.ru/api/getavatar', { picName: response.data.user.picUrl }, {
                                headers: {
                                    'Content-type': 'application/json',
                                    'Authorization': 'Bearer ' + res.data.token
                                }
                            }).then(data => {
                                let payload = {
                                    token: res.data.token,
                                    user: response.data.user,
                                    avatar: data.data.avatar,
                                    currentGroup: {
                                        ...response.data.user._groups[0],
                                        completedTasks: completedTasks,
                                        uncompletedTasks: uncompletedTasks
                                    }
                                }
                                console.log(payload)
                                dispatch({ type: LOGIN_USER_SUCCESS, payload })
                            })
                        } else {
                            let payload = {
                                token: res.data.token,
                                user: response.data.user,
                                avatar: {},
                                currentGroup: {
                                    ...response.data.user._groups[0],
                                    completedTasks: completedTasks,
                                    uncompletedTasks: uncompletedTasks
                                }
                            }
                            dispatch({ type: LOGIN_USER_SUCCESS, payload })
                        }
                    })
                })
            } else {
                dispatch({ type: LOGIN_USER_ERROR })
            }
        }).catch(error => console.log(error))
    }
}

export const logout = () => {
    return (dispatch, getState) => {
        AsyncStorage.removeItem('@easylexlab:jwt').then(e => {
            dispatch({ type: LOGOUT_USER })
        })
    }
}

export const checkLoginState = (jwt) => {
    return (dispatch, getState) => {
        if (jwt != null && jwt != undefined && jwt != '') {
            dispatch({ type: LOGIN_USER_STATE, payload: true })
        } else {
            dispatch({ type: LOGIN_USER_STATE, payload: false })
        }
    }
}

export const fetchUserInfo = jwt => {
    return (dispatch, getState) => {
        if (jwt != null && jwt != undefined && jwt != '') {
            axios.post('http://ealapi.tw1.ru/api/user', {}, {
                headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'Bearer ' + jwt
                }
            }).then(res => {
                let completedTasks = []
                let uncompletedTasks = []
                for (var test of res.data.user._groups[0]._tests) {
                    var done = false
                    if (test.results) {
                        for (let result of test.results) {
                            if (result.username == res.data.user.username) {
                                done = true
                            }
                        }
                        if (!done) {
                            uncompletedTasks.push(test)
                        } else {
                            completedTasks.push(test)
                        }
                    }
                }
                if (res.data.user.picUrl != null && res.data.user.picUrl != undefined) {
                    axios.post('http://ealapi.tw1.ru/api/getavatar', { picName: res.data.user.picUrl }, {
                        headers: {
                            'Content-type': 'application/json',
                            'Authorization': 'Bearer ' + jwt
                        }
                    }).then(response => {
                        let payload = {
                            user: res.data.user,
                            avatar: response.data,
                            currentGroup: {
                                ...res.data.user._groups[0],
                                completedTasks: completedTasks,
                                uncompletedTasks: uncompletedTasks
                            }
                        }
                        //console.log(payload)
                        dispatch({ type: FETCH_USER_INFO, payload })
                    })
                } else {
                    let payload = {
                        user: res.data.user,
                        avatar: {},
                        currentGroup: {
                            ...res.data.user._groups[0],
                            completedTasks: completedTasks,
                            uncompletedTasks: uncompletedTasks
                        }
                    }
                    // console.log(payload)
                    dispatch({ type: FETCH_USER_INFO, payload })
                }
            })
        } else {
            dispatch({ type: LOGIN_USER_STATE, payload: { loggedIn: false } })
        }
    }
}

function sortTasks(group, username) {
    let completedTasks = []
    let uncompletedTasks = []
    for (var test of group._tests) {
        var done = false
        if (test.results) {
            for (let result of test.results) {
                if (result.username == username) {
                    done = true
                }
            }
            if (!done) {
                uncompletedTasks.push(test)
            } else {
                completedTasks.push(test)
            }
        }
    }
    return {
        completedTasks,
        uncompletedTasks
    }
} 